# This is a shell script for creating and pushing the docker image containing suod model. 
# Parameters: ACCOUNT_ID: account_id of user creating the ECR container needed for login
#             REGION: Region of user creating the ECR container needed for login
#             REPO_NAME: repository name for ECR image

ACCOUNT_ID=$1
REGION=$2
REPO_NAME=$3

# build task requires a few minutes to be executed the first time
docker build -f docker/Dockerfile -t $REPO_NAME docker

docker tag $REPO_NAME $ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/$REPO_NAME:latest

 # Get the login command from ECR and execute it directly
$(aws ecr get-login --no-include-email --registry-ids $ACCOUNT_ID --region $REGION)

# Build the docker image locally with the image name and then push it to ECR
aws ecr describe-repositories --repository-names $REPO_NAME || aws ecr create-repository --repository-name $REPO_NAME

docker push $ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/$REPO_NAME:latest




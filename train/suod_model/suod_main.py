import sagemaker
import boto3

from sagemaker import get_execution_role
from sagemaker.session import s3_input

def training_job_suod(args, index, log):
    """ This def creates training job, executes the model training, uses the ECR path 
        to the Docker container for training as parameter for starting a training job.
        Parameters:
        config: dict of defined variables to be used to create the training job.
        index: index of cluster on which model is to be fit.
        Returns the trained suod estimator.
    """
    log.info("Start creating suod training job for cluster_{0}".format(index))
    role = args.role
    region = args.region

    bucket_name = args.bucket
    jobs_folder = args.jobs
    
    hvd_instance_type = args.instance_type
    hvd_processes_per_host = args.process_per_host
    hvd_instance_count = args.instance_count
    train_volume_size= args.volumne_size
    
    # input data location in S3
    train_channel = s3_input(args.training_data_uri + 
                             "/cluster{0}".format(index) + "/train", 
                             compression='Gzip', content_type='text/csv')
    test_channel = s3_input(args.training_data_uri + 
                            "/cluster{0}".format(index) + "/test", 
                            compression='Gzip', content_type='text/csv')
    
    data_channels = {'train': train_channel, 'test': test_channel}

    job_name = args.job_name + '-suod-{}'.format(index)  
    hyperparameters = {
                          "contamination": args.contamination,
                          "n_neighbors": args.n_neigh,
                          "index": index
                      }
    
    input_mode = args.input_mode
    container_image_uri = args.image_uri
    log.info("Input data location in s3 {}".format(args.training_data_uri
                                                + "/cluster{0}".format(index)))
    # create estimator for training job
    suod_estimator = sagemaker.estimator.Estimator(container_image_uri,
                                                    role=role, 
                                                    train_instance_count=hvd_instance_count,
                                                    train_instance_type=hvd_instance_type,
                                                    train_volume_size=train_volume_size,
                                                    input_mode=input_mode,
                                                    output_path='s3://{}/{}/'.format(bucket_name,jobs_folder)
                                                   )
    # set the hyperparameters
    suod_estimator.set_hyperparameters(contamination=hyperparameters['contamination'], 
                                       n_neighbors=hyperparameters['n_neighbors'],
                                       index=hyperparameters['index']
                                      )
    log.info("Executing the training job for SUOD cluster_{0}".format(index))
    suod_estimator.fit(data_channels, wait=False, job_name=job_name)
    
    return suod_estimator


    
    
    
    
    
    
    
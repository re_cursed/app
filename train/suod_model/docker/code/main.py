import argparse
import os
import json
import sys
import time
import pickle
import numpy as np
import tensorflow as tf

from datetime import datetime
from sagemaker_tensorflow import PipeModeDataset
from suod.models.base import SUOD
from pyod.models.lof import LOF
from pyod.models.knn import KNN
from pyod.models.iforest import IForest
from utils import ExitSignalHandler
from utils import write_failure_file, print_json_object, load_json_object, save_model_artifacts, print_files_in_path

hyperparameters_file_path = "/opt/ml/input/config/hyperparameters.json"
inputdataconfig_file_path = "/opt/ml/input/config/inputdataconfig.json"
resource_file_path = "/opt/ml/input/config/resourceconfig.json"
data_files_path = "/opt/ml/input/data/"
failure_file_path = "/opt/ml/output/failure"
model_artifacts_path = "/opt/ml/model/"

training_job_name_env = "TRAINING_JOB_NAME"
training_job_arn_env = "TRAINING_JOB_ARN"

def model(x_train, hyperparameters):
    """Generate a simple model"""
    print("Generating suod model for anomaly detection")
    
    contamination = float(hyperparameters['contamination'])
    n_neighbors = int(hyperparameters['n_neighbors'])
    
    base_estimators = [LOF(contamination=contamination, n_neighbors=n_neighbors),
                       KNN(contamination=contamination, n_neighbors=n_neighbors),
                       IForest(contamination=contamination)
                      ]

    model = SUOD(base_estimators=base_estimators, 
                 n_jobs=10,  # number of workers
                 rp_flag_global=True,  # global flag for random projection
                 bps_flag=False,  # global flag for balanced parallel scheduling
                 approx_flag_global=True,  # global flag for model approximation
                 contamination=contamination # contamination fraction
                )

    model.fit(x_train)
    model.approximate(x_train)
    
    return model

def parse_local(line):
    rdef = [[0.0] for i in range(2,47)]
    select_cols = [i for i in range(2,47)]

    data = tf.io.decode_csv(line, record_defaults=rdef, select_cols=select_cols)
    return data

def _input_fn(channel_name, mode, file_path):
    ds = None
    if mode == 'File':
        ds = tf.data.TextLineDataset(file_path,
                                     compression_type='GZIP').skip(1)
        ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
        ds = ds.map(parse_local)
    else:
        ds = PipeModeDataset(channel=channel_name, record_format='TextLine').skip(1)
        ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
        ds = ds.map(parse_local)
        
    return ds

if __name__ == "__main__":
    
    print("enter")    
    try:
        if os.path.exists(hyperparameters_file_path):
            hyperparameters = load_json_object(hyperparameters_file_path)
            print('\nHyperparameters configuration:')
            print_json_object(hyperparameters)

        if os.path.exists(resource_file_path):
            resource_config = load_json_object(resource_file_path)
            print('\nResource configuration:')
            print_json_object(resource_config)

        if (training_job_name_env in os.environ):
            print("\nTraining job name: ")
            print(os.environ[training_job_name_env])

        if (training_job_arn_env in os.environ):
            print("\nTraining job ARN: ")
            print(os.environ[training_job_arn_env])   
            print("over")

        if os.path.exists(inputdataconfig_file_path):
            input_data_config = load_json_object(inputdataconfig_file_path)
            print('\nInput data configuration:')
            print_json_object(input_data_config)
            train_mode = input_data_config['train']['TrainingInputMode']
            test_mode = input_data_config['test']['TrainingInputMode'] 
            if (train_mode == 'File'):
                for key in input_data_config:
                    print('\nList of files in {0} channel: '.format(key))
                    channel_path = data_files_path + key + '/'
                    if (key == 'train'):
                        train_files = print_files_in_path(channel_path)
                    else:
                        test_files = print_files_in_path(channel_path)

                for file in train_files:
                    x_train = _input_fn('train', train_mode, file)
#                 for file in test_files:
#                     x_test = _input_fn('test', test_mode, file)

            else:
                x_train = _input_fn('train', train_mode, None)
#                 x_test = _input_fn('test', test_mode, None)

            x_train = np.array(list(x_train))
#             x_test = np.array(list(x_test))
            
            print (x_train.shape)
#             print (x_test.shape)
            
            source_suod = model(x_train, hyperparameters)
            print("Going to save model...")
            save_model_artifacts(model_artifacts_path, source_suod, hyperparameters['index'])
            print("Model saved successfully.")
            
    except Exception as e:
        write_failure_file(failure_file_path, str(e))
        print(e, file=sys.stderr)
        sys.exit(1)
import time
import argparse

from logs import logs
from train.autoencoder_model.autoencoder import training_job_autoencoder
from train.lstm_autoencoder_model.lstm_autoencoder import training_job_lstmautoencoder
from train.suod_model.suod_main import training_job_suod

def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--n', type=int, required=True, default=3,
                        help='Number of clusters')
    parser.add_argument('-v', '--verbose', action='count', default=2,
                        help=('Verbosity level. Use -v or -vv or -vvv for '
                              'varying levels of verbosity. Note that -vv '
                              'will be used by default as INFO mode.'))
    parser.add_argument('--bucket', type=str, required=False, 
                        default='sarathor-test',
                        help='S3 bucket where the dataset is stored.')
    parser.add_argument('--jobs', type=str, required=False, default='jobs/test',
                        help='Jobs folder inside S3 where jobs output will be\
                        stored. Default: jobs')
    parser.add_argument('--framework_version', type=str, required=False, 
                        default='2.1.0',
                        help='TensorFlow version to use for executing model\
                        training code. Default: 2.1.0')
    parser.add_argument('--py_version', type=str, required=False, 
                        default='py3',
                        help='Python version for executing the training jobs.\
                        Default: python3')
    parser.add_argument('--input_mode', type=str, required=False, 
                        default='Pipe', choices=['Pipe', 'File'],
                        help='Input mode for the data. Defualt: File')
    parser.add_argument('--training_data_uri', type=str, required=False, 
                        default='s3://sarathor-test/data/ethos12stageaus5/audit/final/clusters',
                        help='Location in S3 where the clustered data is stored.')

    parser.add_argument('--instance_type', type=str, required=False, 
                        default='ml.m5.2xlarge',
                        help='Type of EC2 instance to use for model, \
                        for example, ml.p2.xlarge. Default: ml.p2.xlarge')
    parser.add_argument('--process_per_host', type=int, required=False, 
                        default=1,
                        help='Number of Process per host.')
    parser.add_argument('--instance_count', type=int, required=False, default=1,
                        help='Number of EC2 instances to use. Default: 1')
    parser.add_argument('--volumne_size', type=int, required=False, default=30,
                        help='Size in GB of the EBS volume to use for storing\
                        input data during training (default: 30). Must be \
                        large enough to store training data if File Mode is\
                        used (which is the default).')
    parser.add_argument('--job_name', type=str, required=True, 
                        default="sarathor-anomaly-detection-test4",
                        help='Job Name prefix for training jobs.')
    
    parser.add_argument('--epoch_lstm', type=int, required=False, default=10,
                        help='Number of epochs for LSTM model. Default: 10')
    parser.add_argument('--epoch_auto', type=int, required=False, default=10,
                        help='Number of epochs for Autoencoder model. \
                        Default: 10')
    parser.add_argument('--batch_size_lstm', type=int, required=False, 
                        default=64,
                        help='Batch size for LSTM. Default: 64')
    parser.add_argument('--batch_size_auto', type=int, required=False, 
                        default=64,
                        help='Batch size for Autoencoder. Default: 64')
    parser.add_argument('--contamination', type=float, required=False, 
                        default=0.01,
                        help='Fraction of contaminated data. Default: 0.01')
    parser.add_argument('--n_neigh', type=int, required=False, default=10,
                        help='Number of neighbors to use for suod models. \
                        Default: 10')
    parser.add_argument('--image_uri', type=str, required=False, default=\
                        "340609799170.dkr.ecr.ap-south-1.amazonaws.com/sagemaker-suod-demo1:latest",
                        help='The container image uri to use for training.')
    parser.add_argument('--log_name', type=str, required=False, 
                        default='train', help='Name of log file.')
    parser.add_argument('--role', type=str, required=False, 
                        default="arn:aws:iam::340609799170:role/service-role/AmazonSageMaker-ExecutionRole-20200511T112588",    
                        help='Container Imageuri from ECR for Sagemaker to use for \
                        suod model training.')
    parser.add_argument('--region', type=str, required=False, 
                        default="ap-south-1",
                        help='Region name for AWS account.')
    
    return parser.parse_known_args()


if __name__ == "__main__":
    """ Creates and trains traning job for the three models autoencoder,
        lstm-autoencoder and suod per cluster. Uses the variables defined 
        in args to create the training jobs.
    """
    args, unknown = _parse_args()
    
    log = logs.getLogger(__name__, log_file="logs/{}.log".format(args.log_name), verbosity=args.verbose)
    
    log.info("Start creating training jobs for all the clusters")
    log.info(args)
    
    estimators = []
    for i in range (0,args.n):
        autoencoder_estimator = training_job_autoencoder(args, i, log)
        lstm_autoencoder_estimator = training_job_lstmautoencoder(args, i, log)   
        suod_estimator = training_job_suod(args, i, log)
        estimators.append(suod_estimator)
        estimators.append(autoencoder_estimator)
        estimators.append(lstm_autoencoder_estimator)
        
    log.info("Executing training jobs for all the clusters")
    
    
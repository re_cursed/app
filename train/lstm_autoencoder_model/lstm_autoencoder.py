import os
import sagemaker

from sagemaker.tensorflow import TensorFlow
from sagemaker.session import s3_input

def training_job_lstmautoencoder(args, index, log):
    """ This def creates and executes the training job source code is defined 
        in source_lstm_autoencoder.py. 
        Returns the trained lstm-autoencoder estimator.
        Parameters:
        args: dict of variables to be used to create the lstm-autoencoder 
        trainning job. 
        index: index of cluster on which model is to be fit.
    """
    
    log.info("Start creating lstm-autoencoder training job for cluster_{0}".format(index))
    role = args.role
    region = args.region

    bucket_name = args.bucket
    jobs_folder = args.jobs
    
    hvd_instance_type = args.instance_type
    hvd_processes_per_host = args.process_per_host
    hvd_instance_count = args.instance_count
    train_volume_size= args.volumne_size
    
    # input data location in S3
    train_channel = s3_input(args.training_data_uri + 
                             "/cluster{0}".format(index) + "/train", 
                             compression='Gzip', content_type='text/csv')
    test_channel = s3_input(args.training_data_uri + 
                            "/cluster{0}".format(index) + "/test", 
                            compression='Gzip', content_type='text/csv')
    
    data_channels = {'train': train_channel, 'test': test_channel}
    job_name = args.job_name + '-lstm-autoencoder-{}'.format(index)  
    
    hyperparameters = {
                          "epochs": args.epoch_auto,
                          "batch_size": args.batch_size_auto,
                          "index": index
                      }
    
    py_version = args.py_version
    framework_version = args.framework_version
    input_mode = args.input_mode
    
    log.info("Input data location in s3 {}".format(args.training_data_uri
                                                + "/cluster{0}".format(index)))

    lstm_autoencoder_estimator = TensorFlow(entry_point='train/lstm_autoencoder_model/source_lstm_autoencoder.py',
                                             role=role,
                                             train_instance_count=hvd_instance_count,
                                             train_instance_type=hvd_instance_type,
                                             train_volume_size=train_volume_size,
                                             input_mode=input_mode,
                                             framework_version=framework_version,
                                             py_version=py_version,
                                             script_mode=True,
                                             output_path='s3://{}/{}/'.format(bucket_name,jobs_folder),
                                             hyperparameters=hyperparameters)
    log.info("Executing the training job for LSTM-Autoencoder cluster_{0}".format(index))
    lstm_autoencoder_estimator.fit(data_channels, wait=False, job_name=job_name)
    
    return lstm_autoencoder_estimator
    
    
     
    
    

    
    
   
import argparse
import os
import json
import math
from datetime import datetime
import tensorflow as tf
from sagemaker_tensorflow import PipeModeDataset
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Input, Dropout
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, TensorBoard
from tensorflow.keras.optimizers import Adam
import horovod.tensorflow.keras as hvd

def model(x_train, x_test, args):
    """Generate a simple model"""
    print("Generating AutoEncoder model for anomaly detection")
    print("Epoch: {0} and batch_size: {1}".format(args.epochs, args.batch_size))
    
    for x in x_train.take(1):
        input_dim = x[0].shape[1]
        print("Input Shape is: {0}".format(x[0].shape))

    model = Sequential()
    model.add(Dense(32, input_dim=input_dim, activation='relu'))
    model.add(Dense(16, activation='relu'))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(input_dim))
    model.compile(loss='mse',   
                  optimizer='adam',
                  metrics=['binary_accuracy'])
    model.summary()

    verbose = 1  # if hvd.rank() == 0 else 2

    model.fit(x_train, 
              verbose=verbose, 
              epochs=10, 
              steps_per_epoch=64)
    
    score = model.evaluate(x_test, verbose=verbose)

    print('Test loss: ', score[0])
    print('Test accuracy: ', score[1])

    return model

def parse_local(line):
    rdef = [[0.0] for i in range(2,47)]
    select_cols = [i for i in range(2,47)]
    data = tf.io.decode_csv(line, record_defaults=rdef, select_cols=select_cols)
    data = tf.transpose(data)

    return data, data

def _input_fn(channel_name, mode, num_cpus, base_dir, epochs, batch_size, index):
    ds = None
    if mode == 'File':
        # original file = train_encoded_namespace.csv.gz
        print (os.path.join(base_dir, 'cluster{0}.csv.gz'.format(index)))

        ds = tf.data.TextLineDataset([os.path.join(base_dir, 'cluster{0}.csv.gz'.format(index))],
                                     compression_type='GZIP').skip(1)
        ds = ds.map(parse_local)
        ds = ds.repeat(epochs)
        ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
        ds = ds.batch(batch_size)
    else:
        ds = PipeModeDataset(channel=channel_name, record_format='TextLine').skip(1)
        ds = ds.map(parse_local)
        ds = ds.repeat(epochs)
        ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
        ds = ds.batch(batch_size)
        
    return ds

def _parse_args():
    parser = argparse.ArgumentParser()

    # Data, model, and output directories
    # model_dir is always passed in from SageMaker. By default this is a S3 path under the default bucket.
    parser.add_argument('--model_dir', type=str)
    parser.add_argument('--train', type=str, default=os.environ.get('SM_CHANNEL_TRAIN'))
    parser.add_argument('--test', type=str, default=os.environ.get('SM_CHANNEL_TEST'))
    parser.add_argument('--sm-model-dir', type=str, default=os.environ.get('SM_MODEL_DIR'))
    parser.add_argument('--hosts', type=list, default=json.loads(os.environ.get('SM_HOSTS')))
    parser.add_argument('--current-host', type=str, default=os.environ.get('SM_CURRENT_HOST'))
    parser.add_argument('--num_cpus', type=int, default=os.environ['SM_NUM_CPUS'])
    parser.add_argument('--gpu-count', type=int, default=os.environ['SM_NUM_GPUS'])
    parser.add_argument('--epochs', type=int, default=10)
    parser.add_argument('--batch-size', type=int, default=64)
    parser.add_argument('--index', type=int)
    parser.add_argument('--data_config', type=str, default=os.environ['SM_INPUT_DATA_CONFIG'])
    parser.add_argument('--tensorboard_logs', type=str)
    parser.add_argument('--output_data_dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])

    return parser.parse_known_args()


if __name__ == "__main__":
    args, unknown = _parse_args()

    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    data_config = json.loads(args.data_config)
    train_mode = data_config['train']['TrainingInputMode']
    test_mode = data_config['test']['TrainingInputMode']

    print("Train input mode is: {0}".format(train_mode))
    print("Test input mode is: {0}".format(test_mode))

    x_train = _input_fn('train', train_mode, args.num_cpus, args.train, args.epochs, args.batch_size, args.index)
    x_test = _input_fn('test', test_mode, args.num_cpus, args.test, args.epochs, args.batch_size, args.index)
    
    source_autoencoder = model(x_train, x_test, args)

#     if hvd.rank() == 0:
    print("Going to save model...")
    source_autoencoder.save(os.path.join(args.sm_model_dir, 'model-autoencoder-{}'.format(args.index)), 'source_autoencoder.h5')
    print("Model saved successfully.")
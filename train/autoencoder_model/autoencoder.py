import os
import sagemaker

from sagemaker.tensorflow import TensorFlow
from sagemaker.session import s3_input

def training_job_autoencoder(args, index, log):
    """ 
    This def creates and executes the training job defined in SageMaker 
    Python SDK. Returns the trained autoencoder estimator.
    Parameters:
    config: dict of defined variables to be used to create the training job.
    index: index of cluster on which model is to be fit.
    """
    log.info("Start creating autoencoder training job for cluster_{0}".format(index))
    # setting variables for creating training job in Sagemaker
    role = args.role
    region = args.region

    bucket_name = args.bucket
    jobs_folder = args.jobs
    
    hvd_instance_type = args.instance_type
    hvd_processes_per_host = args.process_per_host
    hvd_instance_count = args.instance_count
    train_volume_size= args.volumne_size
    # input data location in S3
    train_channel = s3_input(args.training_data_uri + 
                             "/cluster{0}".format(index) + "/train", 
                             compression='Gzip', content_type='text/csv')
    test_channel = s3_input(args.training_data_uri + 
                            "/cluster{0}".format(index) + "/test", 
                            compression='Gzip', content_type='text/csv')
    
    data_channels = {'train': train_channel, 'test': test_channel}

    job_name = args.job_name + '-autoencoder-{}'.format(index)
    hyperparameters = {
                          "epochs": args.epoch_auto,
                          "batch_size": args.batch_size_auto,
                          "index": index
                      }
    py_version = args.py_version
    framework_version = args.framework_version
    input_mode = args.input_mode
    log.info("Input data is located in s3 at {}".format(args.training_data_uri
                                                + "/cluster{0}".format(index)))
    # create estimator for training job
    autoencoder_estimator = TensorFlow(entry_point='train/autoencoder_model/source_autoencoder.py',
                                       role=role,
                                       train_instance_count=hvd_instance_count,
                                       train_instance_type=hvd_instance_type,
                                       train_volume_size=train_volume_size,
                                       input_mode=input_mode,
                                       framework_version=framework_version,
                                       py_version=py_version,
                                       script_mode= True,
                                       output_path='s3://{}/{}/'.format(bucket_name,jobs_folder),
                                       hyperparameters=hyperparameters)
    # execute the training job by calling the fit() method
    log.info("Executing the training job for autoencoder cluster_{0}".format(index))
    autoencoder_estimator.fit(data_channels, wait=False, job_name=job_name)

    return autoencoder_estimator
import faiss
import pandas as pd
import numpy as np
import pickle
import time
import boto3
import os

def train_kmeans(df, n, niter):
    """ Trains the kmeans in faiss on df and saves the trained model.
        Parameters: 
        df: Dataframe on which the Faiss has to be trained
        n: Number of clusters to form
        niter: Number of iterations for the k-means algorithm
        bucket: Bucket in S3 where the trained model is to be stored
    """
    col =  [col for col in df.columns if col not in ['audit_id', 'timestamp']]
    # faiss accepts only numpy array 
    df_new = df[col]
    arr = df_new.to_numpy().astype('float32')
    arr = np.ascontiguousarray(arr)

    # train the kmeans model
    kmeans = faiss.Kmeans(arr.shape[1], n, niter=niter, verbose=True)
    kmeans.train(arr)
    faiss.write_index(kmeans.index,'data/encoded_models/kmeans.index')
    
def cluster_index(df):
    """
    Parameters: Dataframe for which index to the most similar index is to be 
    returned.
    Returns the nearest centroid index for each line vector in x in I using 
    the trained model. D contains the squared L2 distances from the centroid. 
    """
    col =  [col for col in df.columns if col not in ['audit_id', 'timestamp']]
    # convert df to numpy array
    df_new = df[col]
    arr = df_new.to_numpy().astype('float32')
    arr = np.ascontiguousarray(arr)
    x = arr.reshape(len(arr),len(arr[1]))
    kmeans = faiss.read_index('data/encoded_models/kmeans.index')
    D, I = kmeans.search(x,1)
    
    return I

def divide_dataset(df, I, n):      
    """ Divides on the basis of index in I and saves the clusters """
    df.reset_index(inplace=True, drop=True)
    # open files to store the clustered data
    l = {}
    for i in range (0,n):
        f = open('data/clusters/cluster{0}.csv'.format(i), 'a+')
        l[i]=f
    
    dx = pd.DataFrame(data=I, columns=['cluster'])
    df_new = pd.concat([df, dx], axis=1)
    # save the rows with cluster index i in cluster{i} file
    for key in range (0, n):
        df1 = df_new.loc[:][df_new.cluster==key]
        df1 = df1.drop(['cluster'], axis=1)
        filename = 'cluster{0}.csv'.format(key)
        df1 = df1.sort_values(by='timestamp', ascending=True)
        df1.to_csv(l[key], header=False, index=False)

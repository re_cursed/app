import pandas as pd
import numpy as np
import category_encoders as ce
import pickle
import ipaddress

def encode(df):
    """ Returns the encoded dataframe using the trained encoders from S3. """
    df = df.sort_values(by='timestamp', ascending=True)
    df_enc = pd.DataFrame()
     
    # copy audit_id and timestamp
    df_enc['audit_id'] = df['audit_id']
    df_enc['timestamp'] = pd.to_datetime(df['timestamp'])
    
    # encode user agent 
    enc_agent = ce.BinaryEncoder(cols='user_agent')
    enc_agent = pickle.load(open('data/encoded_models/encode_agent.pkl','rb'))
    d = enc_agent.transform(df['user_agent'])
    if (len(d.columns)<6):
        for i in range (5, len(d.columns)-1, -1):
            df_enc.insert(len(df_enc.columns), 
                          column="user_agent_{index}".format(index=i), 
                          value=0, allow_duplicates = False)
    df_enc = df_enc.join(d)
        
    # encode reponse_reason 
    enc_reason = ce.BinaryEncoder(cols='response_reason')
    enc_reason = pickle.load(open('data/encoded_models/encode_reason.pkl','rb'))
    d = enc_reason.transform(df['response_reason'])  
    if (len(d.columns) <5):
        for i in range (4, len(d.columns)-1, -1):
            df_enc.insert(len(df_enc.columns), 
                          column="response_reason_{index}".format(index=i), 
                          value=0, allow_duplicates = False)
    df_enc = df_enc.join(d)
        
    # encode response_status
    df_enc['response_status'] = df['response_status'].apply(lambda x: 1 if (x=='Failure' or x=='failure') else 0)
    
    # encode source_ip
    df_enc['source_ips'] = df['source_ips'].apply(lambda x: int(ipaddress.ip_address(x)))
    scaler = pickle.load(open('data/encoded_models/scaler_source_ips.pkl','rb'))
    df_enc['source_ips'] = scaler.fit_transform(np.array(df_enc['source_ips']).reshape(-1,1))
    
    # encode resource_type
    enc_type = ce.BinaryEncoder(cols='resource_type')
    enc_type = pickle.load(open('data/encoded_models/encode_type.pkl','rb'))
    d = enc_type.transform(df['resource_type'])
    if (len(d.columns) <7):
        for i in range (6, len(d.columns)-1, -1):
            df_enc.insert(len(df_enc.columns), 
                          column="resource_type_{index}".format(index = i), 
                          value=0, allow_duplicates = False)  
    df_enc = df_enc.join(d)

    # encode user_name 
    enc_name = ce.BinaryEncoder(cols='user_name')
    enc_name = pickle.load(open('data/encoded_models/encode_name.pkl','rb'))
    d = enc_name.transform(df['user_name'])
    if len(d.columns) < 8: 
        for i in range (7, len(d.columns)-1, -1):
            df_enc.insert(len(df_enc.columns), 
                          column="user_name_{index}".format(index=i), 
                          value=0, allow_duplicates = False)
    df_enc = df_enc.join(d)
    
    # encode response_code
    enc_code = ce.BinaryEncoder(cols='response_code')
    enc_code = pickle.load(open('data/encoded_models/encode_code.pkl','rb'))
    d = enc_code.transform(df['response_code'])  
    if len(d.columns) < 5: 
        for i in range (4, len(d.columns)-1, -1):
            df_enc.insert(len(df_enc.columns), 
                          column="response_code_{index}".format(index=i), 
                          value=0, allow_duplicates = False)
    df_enc = df_enc.join(d)
    
    # encode resource_namespace
    enc_namespace = ce.BinaryEncoder(cols='response_namespace')
    enc_namespace = pickle.load(open('data/encoded_models/encode_namespace.pkl','rb'))
    d = enc_namespace.transform(df['resource_namespace'])  
    if len(d.columns) < 8: 
        for i in range (7, len(d.columns)-1, -1):
            df_enc.insert(len(df_enc.columns), 
                          column="response_namespace_{index}".format(index=i), 
                          value=0, allow_duplicates = False)
    df_enc = df_enc.join(d)
    
    # encode verb
    enc_verb = ce.BinaryEncoder(cols='verb')
    enc_verb = pickle.load(open('data/encoded_models/encode_verb.pkl','rb'))
    d = enc_verb.transform(df['verb'])
    if len(d.columns) < 4: 
        for i in range (7, len(d.columns)-1, -1):
            df_enc.insert(len(df_enc.columns), 
                          column="verb_{index}".format(index=i), 
                          value=0, allow_duplicates = False)
    df_enc = df_enc.join(d)
    
    return df_enc
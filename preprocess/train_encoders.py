import pandas as pd
import numpy as np
import ipaddress
import pickle
import category_encoders as ce
from logs import logs
from sklearn.preprocessing import StandardScaler

def train_and_upload_encoder(df, log, filename, columns):
    """ Trains the enocder and saves to it to S3 bucket """
    encoder = ce.BinaryEncoder(cols=columns)
    encoder = encoder.fit(df)
    log.info("Saving the trained encoder to {}".format(filename))
    pickle.dump(encoder, open('{}'.format(filename),'wb+'))
    
def train_and_upload_ip_encoder(df, log):
    """ Train and upload the trained StandardScaler for encoding of ip_address """
    df_enc = pd.DataFrame()
    df_enc['source_ips'] = df['source_ips'].apply(lambda x: int(ipaddress.ip_address(x)))
    scaler = StandardScaler()
    df_enc['source_ips'] = scaler.fit_transform(np.array(df_enc['source_ips']).reshape(-1,1))         
    log.info("Saving the trained StandardScaler for ip_address to s3")
    pickle.dump(scaler, open('{}'.format('data/encoded_models/scaler_source_ips.pkl'),'wb+'))

def train_encoders_and_upload(unique_items, log):
    """ Parameter: a list of set of unique items in user_agent, verb, 
        resource_namespace, resource_type, reasource_reason, response_code and
        user_name.
        Trains all the encoders for each attribute and the trained encoders are 
        stored in data/encoded_models.
    """
    log.info("Start training the encoder for user agent.")
    arr = np.array(list(unique_items[0]))
    df = pd.DataFrame(data=arr.flatten(), columns=['user_agent'])
    train_and_upload_encoder(df, log, 'data/encoded_models/encode_agent.pkl', 'user_agent')
    
    log.info("Start training the encoder for verb.")
    arr = np.array(list(unique_items[1]))
    df = pd.DataFrame(data=arr.flatten(), columns=['verb'])
    train_and_upload_encoder(df, log, 'data/encoded_models/encode_verb.pkl', 'verb')    
    
    log.info("Start training the encoder for resource_namespace.")
    arr = np.array(list(unique_items[2]))
    df = pd.DataFrame(data=arr.flatten(), columns=['resource_namespace'])
    train_and_upload_encoder(df, log, 'data/encoded_models/encode_namespace.pkl', 'resource_namespace')
    
    log.info("Start training the encoder for resource_type.")
    arr = np.array(list(unique_items[3]))
    df = pd.DataFrame(data=arr.flatten(), columns=['resource_type'])
    train_and_upload_encoder(df, log, 'data/encoded_models/encode_type.pkl', 'resource_type')
    
    log.info("Start training the encoder for response_reason.")
    arr = np.array(list(unique_items[4]))
    df = pd.DataFrame(data=arr.flatten(), columns=['response_reason'])
    train_and_upload_encoder(df, log, 'data/encoded_models/encode_reason.pkl', 'response_reason')
    
    log.info("Start training the encoder for response_code.")
    arr = np.array(list(unique_items[5]))
    df = pd.DataFrame(data=arr.flatten(), columns=['response_code'])
    train_and_upload_encoder(df.astype('int'), log, 'data/encoded_models/encode_code.pkl', 'response_code')
    
    log.info("Start training the encoder for user_name.")
    arr = np.array(list(unique_items[6]))
    df = pd.DataFrame(data=arr.flatten(), columns=['user_name'])
    train_and_upload_encoder(df, log, 'data/encoded_models/encode_name.pkl', 'user_name')
    
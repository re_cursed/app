import pandas as pd
import os
import argparse
import sys
from logs import logs
from sklearn.model_selection import train_test_split
from preprocess.cluster import cluster_index, train_kmeans, divide_dataset
from preprocess.train_encoders import train_encoders_and_upload, train_and_upload_ip_encoder
from preprocess.encode import encode
from utils.utils import *

def feature_extraction(bucket, folder_to_read_from, folder_to_write_to):     
    """ Select relevant attributes from input data, performs data_cleaning and
        upload the output to s3. 
        Parameters: 
        bucket: bucket from which data is to be extracted
        folder_to_read_from: folder path in S3 where data is located
        folder_to_write_to: folder path in S3 where cleaned data is to be stored
        Returns: unique values from selected columns to be used to train the encoders. 
    """
    # list of columns to extract from df
    cols_list = ['audit_id', 'source_ips', 'verb', 'user_agent', \
                'resource_namespace', 'resource_type', 'response_code', \
                'response_reason', 'response_status', 'user_name', 'timestamp']
    
    
    response = iter_over_files_in_s3_folder(bucket, folder_to_read_from)
    # for storing the unique values in seven columns on which encoders will be trained
    unique_items = [set() for x in range(7)]
    count=1
    # iterate over all the files and perform datacleaning 
    if int(response.get('KeyCount')) > 0:
        for fileobj in response['Contents']:
            log.info("Starting Feature Extraction for {}".format(fileobj['Key']))
            df = read_csv_from_s3(fileobj, bucket, log, cols_list)
            log.debug("Read {} for feature extraction.".format(fileobj['Key']))
            df = data_cleaning(df)
            log.info("Data cleaning completed for {}".format(fileobj['Key']))
            li = get_unique(df)
            for i in range (0,7):
                unique_items[i] = unique_items[i] | li[i]
            path = folder_to_write_to + "{}.csv.gz".format(count)
            write_dataframe_to_csv_compressed_on_s3(df, bucket, path, log)
            count=count+1
    
    return unique_items

def data_cleaning(df):
    """ Removes the irrelevant data from df and set defualt inplace of missing values """
    cols_list = ['audit_id', 'source_ips', 'verb', 'user_agent', \
                'resource_namespace', 'resource_type', 'response_code', \
                'response_reason', 'response_status', 'user_name', 'timestamp']
    # drop all rows with NULL values in specified columns
    df.dropna(axis=0, inplace=True, how='any', 
              subset=['source_ips','verb','user_agent', 'resource_namespace',
                      'timestamp', 'user_name','response_code','audit_id'])  
    df.drop_duplicates(['audit_id'], inplace=True)
    df.drop_duplicates(['timestamp'], inplace=True)
    # remove timestamp which has auditid string in it
    df['timestamp'] = df['timestamp'].apply(lambda x: -1 if ('audit_id' in x) else x)
    df = df[df['timestamp']!=-1]
    df['resource_type'].fillna('default', inplace=True)
    df['response_reason'].fillna('default', inplace=True)
    df['response_status'].fillna('Success', inplace=True)
     # remove all rows where col header is equal to data in df  
    for col in cols_list:
        df = df[~df[col].isin(cols_list)]
    # consider only the first part of user_agent
    df['user_agent'] = df['user_agent'].apply(lambda x: (x.split('/',1))[0])                   
    
    return df

def get_unique(df):
    """ Returns all the column-wise unique values in given df in the form of list of sets. """
    unique_items =  [[] for x in range(7)]
    unique_items[0] = set(df['user_agent'].unique().flatten())
    unique_items[1] = set(df['verb'].unique().flatten())
    unique_items[2] = set(df['resource_namespace'].unique().flatten())
    unique_items[3] = set(df['resource_type'].unique().flatten())
    unique_items[4] = set(df['response_reason'].unique().flatten())
    unique_items[5] = set(df['response_code'].unique().flatten())
    unique_items[6] = set(df['user_name'].unique().flatten())
    
    return unique_items

def get_data(bucket, folder):
    """ Returns the Dataframe read from all the files in given folder in S3 
        Parameters: 
        bucket: bucket name from where data is to be read
        folder: path in S3 where data is located 
    """
    response = iter_over_files_in_s3_folder(bucket, folder)
    df_result = pd.DataFrame()
    # read the files and concat the df with df_result
    if int(response.get('KeyCount')) > 0:
        for fileobj in response['Contents']:
            df = read_csv_from_s3(fileobj, bucket, log, None)
            df_result = pd.concat([df, df_result], ignore_index=True)
    else:
        log.info("No file found in {}.".format(folder))
        
    return df_result

def encode_data(folder_to_read_from, bucket, folder_to_write):
    """ Encodes the data in folder_to_read_from and uploads the encoded data to 
        folder_to_write in S3 bucket. 
        Parameters: 
        bucket: bucket from which data is to be extracted and written back
        folder_to_read_from: folder path in S3 where data is located
        folder_to_write_to: folder path in S3 where encoded data is to be stored
    """
    response = iter_over_files_in_s3_folder(bucket, folder_to_read_from)
    df_result = pd.DataFrame()
    # encode the files one by one
    count=1
    if int(response.get('KeyCount')) > 0:
        for fileobj in response['Contents']:
            df = read_csv_from_s3(fileobj, bucket, log, cols_list=None)
            df = encode(df)
            path = folder_to_write + "{}.csv.gz".format(count)
            write_dataframe_to_csv_compressed_on_s3(df, bucket, path, log)
            count=count+1
    else:
        log.info("No file found in {}.".format(folder_to_read_from))
            

def cluster_data(folder, n, bucket):
    """ Create n clusters of data in all the files in given folder and store 
        clusters locally. 
        Parameters: 
        n: number of clusters to form
        bucket: name of bucket where data is located in S3
        folder: path in S3 where the encoded data is stored
    """  
    response = iter_over_files_in_s3_folder(bucket, folder)
    df_result = pd.DataFrame()
    # form n clusters of the encoded files 
    if int(response.get('KeyCount')) > 0:
        for fileobj in response['Contents']:
            df = read_csv_from_s3(fileobj, bucket, log, cols_list=None)
            I = cluster_index(df)
            divide_dataset(df, I, n)
        log.info("Clustering of data in {} clusters is completed.".format(n))
    else:
        log.info("No file found in {}.".format(folder))
            

def write_clusters_to_s3(n, bucket, folder, data_type):
    """ Split the clustered data in train and test. This will create one folder for
        each cluster and each cluster will contain  a folder for test and train data. 
        Parameters: 
        n: number of clusters 
        bucket: name of bucket where data is to be stored
        folder: location in S3 where data is to be stored
    """
    for i in range (0, n):
        file = 'data/clusters/cluster{0}.csv'.format(i)
        df_cluster = pd.read_csv(file, index_col=False)
        if (data_type == 'Train'):
            df_cluster = train_test_split(df_cluster, test_size=0.33, shuffle=1)
            df_train = df_cluster[0].sort_values(by='timestamp', ascending=True)
            df_test = df_cluster[1].sort_values(by='timestamp', ascending=True)
            write_dataframe_to_csv_compressed_on_s3(df_train, bucket, 
                                                    '{0}/cluster{1}/train/cluster{1}.csv.gz'.format(folder, i), log)
            write_dataframe_to_csv_compressed_on_s3(df_test, bucket, 
                                                    '{0}/cluster{1}/test/cluster{1}.csv.gz'.format(folder, i), log)   
            log.info('Removing the cluster data locally.')
            os.remove(file)
        else:
            df_train = df_cluster.sort_values(by='timestamp', ascending=True)
            write_dataframe_to_csv_compressed_on_s3(df_train, bucket, 
                                                    '{0}/cluster{1}/test/cluster{1}.csv.gz'.format(folder, i), log)
            log.info('Removing the cluster data locally.')
            os.remove(file)
            
def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--n', type=int, required=False, default=3, 
                        choices=range(1,20),
                        help='Number of clusters in which training data will \
                        be divided.')
    parser.add_argument('--n_iter', type=int, default=10, choices=range(1,25), 
                        required=False,
                        help='Maximum number of iterations for the clustering \
                        algorithm training. Default: 20')
    parser.add_argument('--folder', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/test/test_data/',
                        help='S3 Location where the dataset is stored.')
    parser.add_argument('--bucket', type=str, required=False, 
                        default='sarathor-test',
                        help='S3 bucket where the dataset is stored.')
    parser.add_argument('-v', '--verbose', action='count', default=2, 
                        help=('Verbosity level. Use -v or -vv or -vvv for '
                              'varying levels of verbosity. Note that -vv '
                              'will be used by default as INFO mode.'))
    parser.add_argument('--clear_loc', type=str, 
                        default='data/ethos12stageaus5/audit/test/clear_data/',
                        help='S3 location where the cleaned dataset is to be stored.')
    parser.add_argument('--encoded_folder', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/test/encoded_data/',
                        help='S3 Location where the encoded dataset is to be stored.')
    parser.add_argument('--clusters_folder', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/test/clusters',
                        help='Folder Location in S3 where clustered dataset is \
                        to be stored.')
    parser.add_argument('--encoded_models', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/final/encoded_models',  
                        help='Folder Location in S3 where encoded models are stored \
                        in case of testing data or to be stored in case of training.')
    parser.add_argument('--data_type', type=str, required=False, 
                        default='Test', choices=['Train', 'Test'],
                        help='Type of data to preprocess.')
    parser.add_argument('--log_name', type=str, required=False, 
                        default='preprocess_test', 
                        help='Name of log file.')
    
    
    return parser.parse_known_args()

if __name__ == "__main__":
    args, unknown = _parse_args()
    
    log = logs.getLogger(__name__, log_file="logs/{}.log".format(args.log_name), 
                         verbosity=args.verbose)
    
    log.info("Starting the preprocessing of data.")
    
    # feature extraction
    log.info("Starting feature extraction from all files in {} in S3.".format(args.folder))
    unique_items = feature_extraction(args.bucket, args.folder, args.clear_loc)
    log.info("Completed feature extraction from all files in {} in S3!".format(args.folder))
    log.debug("List of list of unique items for all columns",unique_items)
    
    if (args.data_type=='Train'):
        # train the encoders
        log.info("Training the encoders.")
        train_encoders_and_upload(unique_items, log)
        log.info("Reading all files from {} to train the Standard Scaler".format(args.clear_loc))
        df = get_data(args.bucket, args.clear_loc)
        log.info("Training the StandardScaler.")
        train_and_upload_ip_encoder(df, log)
        log.info("Training for all the encoders completed!")
    else:
        # download all the encoders from S3 to use for encoding of dataset
        get_folder_from_s3(args.bucket, args.encoded_models, log)
    
    # encode the cleaned data
    log.info("Start encoding the dataset")
    encode_data(args.clear_loc, args.bucket, args.encoded_folder)
    log.info("Encoding of the dataset completed!")
    
    if (args.data_type=='Train'):
        # train the faiss kmeans algorithm 
        log.info("Training the Faiss K-means on the input data")
        df = get_data(args.bucket, args.encoded_folder)
        train_kmeans(df, args.n, args.n_iter)
        log.info("Completed training of kmeans algorithm!")
    
    if (args.data_type=='Train'):
        # upload the trained encoders to S3
        upload_folder_to_s3('data/encoded_models', args.bucket, args.encoded_models, log)
        # create CSV to store the clusters
        log.info("Create {} csv for storing the clusters of data locally".format(args.n))
        create_csv_for_clusters(log, args.n, df.columns)
    else:
        # create CSV to store the clusters
        log.info("Create {} csv for storing the clusters of data locally".format(args.n))
        create_csv_for_clusters_test(log, args.n)

    # cluster the dataset 
    log.info("Preparing clusters of the dataset")
    cluster_data(args.encoded_folder, args.n, args.bucket)
    
    # write all the clusters to s3
    log.info("Start writing the clusters to S3.")
    write_clusters_to_s3(args.n, args.bucket, args.clusters_folder, args.data_type)
    log.info("Feature Extraction completed for the data for {}.".format(args.folder))
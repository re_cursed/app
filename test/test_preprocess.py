import unittest
import pandas as pd
import numpy as np
import argparse
from preprocess import preprocess

def _parse_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--tests_file', type=str, required=False, 
                        default='data/test_data/test_clean.csv', 
                        help='Name of log file.')
    parser.add_argument('--clean_data', type=str, required=False, 
                        default='data/test_data/clean_data.csv', 
                        help='Name of log file.')
    parser.add_argument('--bucket', type=str, required=False, 
                        default='sarathor-test', 
                        help='Name of log file.')
    parser.add_argument('--encoded_models', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/final/encoded_models', 
                        help='Name of log file.')
    return parser.parse_known_args()

  
def test_cleaning(self):
        
        test_file = 'data/test_data/test_clean.csv'
        clean_file = 'data/test_data/clean_data.csv'
        
        df = pd.read_csv(test_file)                                               
        df_result = preprocess.data_cleaning(df)
        df_res = pd.read_csv(clean_file)
        
        pd.testing.assert_frame_equal(df_res.reset_index(drop=True), 
                                      df_result.reset_index(drop=True),
                                      check_dtype=False) 
            
def test_encoding(self):

        df = pd.read_csv(self.args.clean_file)

        get_folder_from_s3(self.args.bucket, self.args.encoded_models, logs)
        df_enc =  preprocess.encode.encode(df)

        df_enc.to_csv('data/test_data/encoded_data.csv', index=False)
        df_result = pd.read_csv('data/test_data/encoded_data.csv')

        pd.testing.assert_frame_equal(df_enc.reset_index(drop=True), 
                                      df_result.reset_index(drop=True),
                                      check_dtype=False)  
        
        
  
            


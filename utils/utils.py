import pandas as pd
import os
import time
import boto3
from logs import logs

def read_csv_from_s3(response, bucket, log, cols_list=None):
    """ Read a CSV from S3 and return the dataframe """
    s3 = boto3.client('s3')
    fr_file = s3.get_object(Bucket=bucket, Key=response['Key'])
    if (response['Key'].endswith('gz')):
        log.info("Reading the zipped file {} from s3".format(response['Key']))
        df = pd.read_csv(fr_file['Body'], compression='gzip', 
                         error_bad_lines=False, usecols=cols_list)
        return df 
    elif (response['Key'].endswith('csv')):
        log.info("Reading the csv file {} from s3".format(response['Key']))
        df = pd.read_csv(fr_file['Body'],
                         error_bad_lines=False, 
                         usecols=cols_list)
        return df 
    else:
        log.info("File to be read is not in CSV format.")
    

def iter_over_files_in_s3_folder(bucket, prefix):
    """ Returns the response dict for S3 folder """
    s3 = boto3.client('s3')
    response = s3.list_objects_v2(Bucket=bucket, Prefix = prefix, MaxKeys=100)
    return response
    
def write_dataframe_to_csv_compressed_on_s3(dataframe, bucket, filename, log):
    """ Write a dataframe to a CSV on S3 """
    # Write dataframe to buffer
    log.info('Generating temp filename to save CSV on S3')
    t1 = time.time()
    tmp_file = 'tmp_' + str(time.time()) + '.csv.gz'
    dataframe.to_csv(tmp_file, compression='gzip', sep=",", index=False)
    t2 = time.time()
    log.info('Created tar file {0} from dataframe in {1} seconds'.format(tmp_file, t2-t1))
    s3 = boto3.client('s3')
    with open(tmp_file, "rb") as f:
        s3.upload_fileobj(f, bucket, filename)
    log.info("Writing {} records to {}".format(len(dataframe), filename))
    #Perform cleanup
    os.remove(tmp_file) 

def create_csv_for_clusters_test(log, n):
    """ Create n CSV for storing the clustered dataset """
    columns=['audit_id', 'timestamp']
    for i in range(2,47):
        columns.append('column_{}'.format(i))
    
    for i in range (0,n):
        with open('{0}/cluster{1}.csv'.format('data/clusters',i), 'w+') as f:
            df = pd.DataFrame(columns=columns)
            df.to_csv(f, header=True, index=False)
    log.info("{} CSV are created in {}.".format(n, 'data/clusters'))
    
def create_csv_for_clusters(log, n, cols):
    """ Create n CSV for storing the clustered dataset """
    for i in range (0,n):
        with open('{0}/cluster{1}.csv'.format('data/clusters',i), 'w+') as f:
            df = pd.DataFrame(columns=cols)
            df.to_csv(f, header=True, index=False)
    log.info("{} CSV are created in {}.".format(n, 'data/clusters'))
    
# def remove_clustered_data(folderpath):
#     # remove all the clusters locally
#     files = os.listdir(folderpath)
#     for file in files:
#         os.remove(folderpath+ '/' + file)
        
def download_csv_from_s3(bucket, folder, response, log):
    """ Read the CSV from S3 and store the CSV in a temp file 
        Returns the temp file 
    """
    s3 = boto3.client('s3')
    fr_file = s3.get_object(Bucket=bucket, Key=response['Key'])
    if (response['Key'].endswith('gz')):
        log.info("Reading the zipped file {} from s3".format(response['Key']))
        df = pd.read_csv(fr_file['Body'], compression='gzip', error_bad_lines=False)
        t1 = time.time()
        tmp_file = 'tmp_' + str(time.time()) + '.csv.gz'
        df.to_csv(tmp_file, compression='gzip', sep=",", index=False)
        return tmp_file
    elif (response['Key'].endswith('csv')):
        log.info("Reading the csv file {} from s3".format(response['Key']))
        df = pd.read_csv(fr_file['Body'], error_bad_lines=False)
        t1 = time.time()
        tmp_file = 'tmp_' + str(time.time()) + '.csv.gz'
        df.to_csv(tmp_file, compression='gzip', sep=",", index=False)
        return tmp_file
    else:
        log.info("File to be read is not in CSV format.")
        
def upload_folder_to_s3(folder, bucket, s3_folder, log):
    """ Write all the trained encoders in folder to s3_folder """
    files = os.listdir(folder)
    s3 = boto3.client('s3')
    for file in files:
        if (file!='.ipynb_checkpoints'):
            log.info("Writing {} file to {}".format(file, s3_folder+ '/' + file))
            filepath = folder + '/' + file
            with open(filepath, "rb") as f:
                s3.upload_fileobj(f, bucket, s3_folder+ '/' + file)

def get_folder_from_s3(bucket, folder, log):
    s3 = boto3.resource('s3')
    response = iter_over_files_in_s3_folder(bucket, folder)
    if int(response.get('KeyCount')) > 0:
        for fileobj in response['Contents']:
            fi = fileobj['Key'].split('/')[-1]
            log.info("Downloading file {} from S3.".format(fi))
            s3.meta.client.download_file(bucket, fileobj['Key'], 
                                         'data/encoded_models/{}'.format(fi))
         
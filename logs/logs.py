import logging

def setup_file_logger(log_file, verbosity):
    if verbosity == 1:
        log_level = logging.ERROR
    elif verbosity == 2:
        log_level = logging.INFO
    else:
        log_level = logging.DEBUG
    format = "%(asctime)s,%(msecs)03d [%(levelname)-5s] [%(name)s:%(lineno)d]  %(message)s"
    logging.basicConfig(format=format, level=log_level, datefmt="%Y-%m-%d %H:%M:%S", filename=log_file)

def getLogger(logger_name=None, log_file='logs/iqr.log', verbosity=2):
    setup_file_logger(log_file, verbosity)
    return logging.getLogger(logger_name)
import argparse
import os
import json
import sys
import time
import pickle
import numpy as np
import tensorflow as tf

from datetime import datetime
from suod.models.base import SUOD
from pyod.models.lof import LOF
from pyod.models.knn import KNN
from pyod.models.iforest import IForest

def parse_local(line):
    """ Parse input line """
    rdef = [[0.0] for i in range(2,47)]
    select_cols = [i for i in range(2,47)]

    data = tf.io.decode_csv(line, record_defaults=rdef, select_cols=select_cols)
    return data


def parse_local_id(line):
    """ Parse id from line """
    rdef = [["audit_id"] for i in range(0,1)]
    select_cols = [i for i in range(0,1)]
    data = tf.io.decode_csv(line, record_defaults=rdef, select_cols=select_cols)

    return data

def _input_fn(file):
    """ Extracts data from file. Returns dataset and id corresponding to each datapoint."""
    ds = None
    ds = tf.data.TextLineDataset(file, compression_type='GZIP').skip(1)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    ds_id = ds.map(parse_local_id)
    ds_id = list(ds_id.as_numpy_iterator())
    ds = ds.map(parse_local)
        
    return ds, ds_id

def anomaly(data_id, predicted_labels, predicted_prob):
    """ Computes errors for data """
    anomalies = []
    seq = zip(predicted_labels, data_id, predicted_prob)
    for label, data_id , prob in seq:
        if ((label[0]+label[1]+label[2])>=2):
            anomalies.append((data_id[0].decode('utf-8'), max(prob)))
            
    return anomalies

def find_anomalies_model_suod(filename, suod_model, log):
    """ Parameters:
        filename: path from where the model is to be read
        suod_model: model which is to be used for predictions
    """
    log.info("Reading the input dataset from {}".format(filename))
    dataset, ds_id = _input_fn(filename)
    test = np.array(list(dataset))
    log.info("Predicting labels for each data point.")
    predicted_labels = suod_model.predict(test)  # predict labels
    log.info("Predicting probabilities for each data point to belong to a class.")
    predicted_prob = suod_model.predict_proba(test)
    log.info("Finding anomalous points from dataset.")
    pred_suod = anomaly(ds_id, predicted_labels, predicted_prob)    

    return pred_suod

import pandas as pd
import numpy as np
import time
import os
import boto3
import argparse
import awswrangler as wr

from logs import logs
from utils.utils import *

def create_dataframe_from_athena(database, audit_id):
    """ Create a dataframe from athena using SQL from database and table test_meta
        Returns all the columns of the original data corresponding to each audit_id
    """
    sql_query = "select * from test_meta where audit_id in (\'" +"\',\'".join((str(n[0]) for n in audit_id)) + "\')"
    sess = wr.Session(athena_ctas_approach=True)
    ns_df = sess.pandas.read_sql_athena(
        sql=sql_query,
        database=database
    )
    return ns_df    
      
def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='count', default=2, 
                        help=('Verbosity level. Use -v or -vv or -vvv for '
                              'varying levels of verbosity. Note that -vv '
                              'will be used by default as INFO mode.'))
    parser.add_argument('--bucket', type=str, required=False, 
                        default='sarathor-test',
                        help='S3 bucket where the dataset is stored.')
    parser.add_argument('--folder', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/final/anomalous_data',
                        help='S3 bucket where the anamolous data id is stored.')
    parser.add_argument('--map_folder', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/final/mapped_data',
                        help='S3 folder where the mapped anomalous data is to be stored.')
    parser.add_argument('--database', type=str, required=False, 
                        default='sarathor-test',
                        help='Database to be used for athena query.')
    
    return parser.parse_known_args()
    
if __name__ == "__main__":
        
    args, unknown = _parse_args()    
    
    log = logs.getLogger(__name__, log_file="logs/map.log", verbosity=args.verbose)
    
    log.info("Start mapping the anomlies to the original data.")
    
    response = iter_over_files_in_s3_folder(args.bucket, args.folder)
    df_result = pd.DataFrame()
    
    if int(response.get('KeyCount')) > 0:
        for fileobj in response['Contents']:
            df = read_csv_from_s3(fileobj, args.bucket, log, cols_list=['audit_id'])
            df.drop_duplicates(['audit_id'], inplace=True)
            audit_id = df.values.tolist()
            log.info("Creating dataframe from athena for the data")
            ns_df = create_dataframe_from_athena(args.database, audit_id)
            df_result = pd.concat([df_result, ns_df], ignore_index=True)
            
    filename = str(time.time())+'.csv'
    log.info("Writing Dataframe to a CSV at {}".format(filename))
    df_result.drop_duplicates(['audit_id'], inplace=True,  keep=False)
    df_result.drop_duplicates(['timestamp'], inplace=True,  keep=False)
    df_result.sort_values(by='timestamp', ascending=True)
    # final CSV containing original data columns for all anamalous data points 
    df_result.to_csv('data/mapped_data/{}'.format(filename), index=False)
    
    # upload the final results to S3
    s3 = boto3.client('s3')
    with open('data/mapped_data/{}'.format(filename), "rb") as f:
        s3.upload_fileobj(f, args.bucket, args.map_folder + '/' + filename)
       
            
        
import os
import boto3
import time 
import pickle
import tensorflow as tf
import pandas as pd
import argparse
from logs import logs
from utils.utils import *
from inference.predict_auto import find_anomalies_model_auto
from inference.predict_lstm import find_anomalies_model_lstm
from inference.predict_suod import find_anomalies_model_suod

def calculate_score_for_anomalies(index, anomaly_list, data_type, log):
    """ Calculate score for anomalies in anomaly_list """
    # a dict to store the score values corresponding to each id for 
    # all the three algorithms, key value is id and value is list 
    # of scores from the models
    score = {}    
    for li in (anomaly_list):
        for i,j in li:
            if (i not in score):
                score[i] = [j]
            else:
                score[i].append(j)

    anomalies={}  # the key is id and value is score 
    for key in score.keys():
        if (len(score[key])>=2):
            score[key].sort(reverse=True)
            anomalies[key] = (score[key][0] + score[key][1])/2            
    
    df = pd.DataFrame(anomalies.items(), columns=['audit_id', 'score'])
    if (data_type=='Test'):
        log.info("Common Anomalies from the three models are {}.".format(len(df)))
        df.to_csv('data/anomalous_data/anomalies_test_{}.csv'.format(index), index=False)
    else:
        log.info("Common Anomalies from the three models are {}.".format(len(df)))
        df.to_csv('data/anomalous_data/anomalies_train_{}.csv'.format(index), index=False)
    
def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--n', type=int, required=True, default=8, 
                        choices=range(1,20), help='Number of clusters')
    parser.add_argument('-v', '--verbose', action='count', default=2, 
                        help=('Verbosity level. Use -v or -vv or -vvv for '
                              'varying levels of verbosity. Note that -vv '
                              'will be used by default as INFO mode.'))
    parser.add_argument('--bucket', type=str, required=False, 
                        default='sarathor-test',
                        help='S3 bucket where the dataset is stored.')
    parser.add_argument('--folder', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/test/clusters/',
                        help='File path in S3 where the dataset is located.')
    parser.add_argument('--s3_folder', type=str, required=False, 
                        default='data/ethos12stageaus5/audit/test/anomalous_data',
                        help='File path in S3 where the anomalous dataset is to be stored.')
    parser.add_argument('--k1', type=int, required=False, default=3,
                        help='k for calculating threshold in autoencoder.')
    parser.add_argument('--k2', type=int, required=False, default=3,
                        help='k for calculating threshold in lstm-autoencoder.')
    parser.add_argument('--batch_size1', type=int, required=False, default=64,
                        help='batch size for Autoencoder.')
    parser.add_argument('--batch_size2', type=int, required=False, default=64,
                        help='batch size for lstm-autoencoder model.')
    parser.add_argument('--data_type', type=str, required=False, default='Test',
                        choices=['Train','Test'],
                        help='Data type is test/ training data.')
    parser.add_argument('--log_name', type=str, required=False, default='inference',
                        help='Name of log file.')
    return parser.parse_known_args()
    
    
if __name__ == "__main__":
    """ Creates and trains traning job for the three models autoencoder,
        lstm-autoencoder and suod per cluster. Uses the variables defined 
        in args to create the training jobs.
    """
    args, unknown = _parse_args()    
    
    log = logs.getLogger(__name__, log_file="logs/{}.log".format(args.log_name), verbosity=args.verbose)
    
    log.info("Starting inference for data.")
    anomalies = []

    for i in range (0, args.n):
        log.info("Running for cluster {}".format(i))
        test_folder = args.folder + 'cluster{}/'.format(i)+ 'test/'
        if(args.data_type=='Train'):
            train_folder = args.folder + 'cluster{}/'.format(i)+ 'train/'
        log.info("Loadind Autoencoder model")
        auto_model = tf.keras.models.load_model("data/trained_models/model-autoencoder-{}".format(i))
        log.info("Loading LSTM Autoencoder model")
        lstm_model = tf.keras.models.load_model("data/trained_models/model-lstm-autoencoder-{}".format(i))
        log.info("Loading SUOD model.")
        suod_model = pickle.load(open("data/trained_models/source_suod_{}.pkl".format(i), 'rb'))
        folders = [] 
        folders.append(test_folder)
        if(args.data_type=='Train'):
            folders.append(train_folder)
        
        for folder in folders:       
            response = iter_over_files_in_s3_folder(args.bucket, folder)
     
            if int(response.get('KeyCount')) > 0:
                for fileobj in response['Contents']:
                    log.info("Start finding anomalies from {}".format(fileobj['Key']))
                    log.info("Reading the CSV {} from S3 and Store the dataframe \
                    in a temp file".format(fileobj['Key']))
                    anomaly_list = []
                    # download the CSV to read from S3.
                    filename = download_csv_from_s3(args.bucket, args.folder, fileobj, log)
                    log.info("Finding anomalies using Autoencoder model.")
                    anomalies_list1 = find_anomalies_model_auto(filename, auto_model,
                                                                args.k1, args.batch_size1, log)
                    anomaly_list.append(anomalies_list1)
                    log.info("Anomalies detected by Autoencoder {}".format(len(anomalies_list1)))
                    log.info("Found anomalies from {} using Autoencoder model for \
                              cluster {}.".format(fileobj['Key'], i))
                    
                    log.info("Finding anomalies using LSTM-Autoencoder model.")
                    anomalies_list2 = find_anomalies_model_lstm(filename, lstm_model,
                                                                args.k2, args.batch_size2, log)
                    anomaly_list.append(anomalies_list2)
                    log.info("Found anomalies from {} using LSTM-Autoencoder model \
                              for cluster {}.".format(fileobj['Key'], i))
                    log.info("Anomalies detected by LSTM-Autoencoder {}".format(len(anomalies_list2)))
                    
                    log.info("Finding anomalies using SUOD model.")
                    anomalies_list3 = find_anomalies_model_suod(filename, suod_model, log)
                    log.info("Found anomalies from {} using SUOD model \
                              for cluster {}.".format(fileobj['Key'], i))   
                    anomaly_list.append(anomalies_list3)
                    log.info("Anomalies detected by SUOD {}".format(len(anomalies_list3)))
                    log.info("Remove the temp file")
                    os.remove(filename)
                    # calculate final anomaly score 
              
                    if (folder==test_folder):  
                        calculate_score_for_anomalies(i, anomaly_list, 'Test', log)
                    else:
                        calculate_score_for_anomalies(i, anomaly_list, 'Train', log)
      
    # upload all the CSV containing anomalous points on S3 
    upload_folder_to_s3('data/anomalous_data', args.bucket, args.s3_folder, log)                
                 
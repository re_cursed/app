import tensorflow as tf
import numpy as np
import os

def parse_local(line):
    """ Parse input line """
    rdef = [[0.0] for i in range(2,47)]
    select_cols = [i for i in range(2,47)]
    data = tf.io.decode_csv(line, record_defaults=rdef, select_cols=select_cols)
    
    return data

def parse_local_id(line):
    """ Parse id from line """
    rdef = [['audit_id'] for i in range(0,1)]
    select_cols = [i for i in range(0,1)]
    data = tf.io.decode_csv(line, record_defaults=rdef, select_cols=select_cols)

    return data

def _input_fn(filepath, batch_size):
    """ Extracts data divides elements of this dataset into batches. """
    ds = None
    ds_id = None
    ds = tf.data.TextLineDataset(filepath, compression_type='GZIP').skip(1)
    ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
    ds_id = ds.map(parse_local_id)
    ds_id = list(ds_id.as_numpy_iterator())
    ds = ds.map(parse_local)
    windowds = list(ds.as_numpy_iterator())
    ll = []
    for i in range(len(windowds)-29):
        ll.append(windowds[i:(i+30)])
    dataset = tf.data.Dataset.from_tensor_slices(ll).batch(batch_size)
        
    return dataset, ds_id

def errors(dataset, model):
    """ Computes errors for data """
    err = []
    for x_data in (dataset.as_numpy_iterator()):
        y_data = model.predict(x_data)
        seq = zip(x_data, y_data)
        for x, y in seq:
            mse = tf.keras.losses.MeanSquaredError()
            err.append(mse(x, y).numpy())
    print("error done")
    return err

def threshold(errors, k):
    """ Computes threshold """ 
    data_mean = np.mean(errors)
    data_std = np.std(errors)
    anomaly_cut_off = data_std * k
    upper_limit = data_mean + anomaly_cut_off
    print("thre done")

    return upper_limit

def anomaly(upper_limit, errors, data_id):
    """ Returns anomalous data id using the threshold """
    anomalies = []
    seq = zip(errors, data_id)
    max_err = max(errors)
    for err, data_id in seq:
        if err > upper_limit:
            anomalies.append((data_id[0].decode('utf-8'), err/max_err))

    return anomalies

def find_anomalies_model_lstm(filename, model, k, batch_size, log):
    """ Parameters:
        filename: path from where the model is to be read
        model: model which is to be used for predictions
        k: k to use for threshold calculation
        batch_size: batch_size for the data
        Returns the audit_id of the anomolous points
    """
    log.info("Reading the input dataset from {}".format(filename))
    dataset, ds_id = _input_fn(filename, batch_size)
    log.info("Calculating errors")
    err = errors(dataset, model)
    log.info("Calculating threshold for the dataset.")
    thre = threshold(err, k)
    log.info("Calculated threshold is {}".format(thre))
    log.info("Find anomalous data points from dataset.")
    pred_lstm = anomaly(thre, err, ds_id)
    
    return pred_lstm
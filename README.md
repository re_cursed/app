# Anomaly detection Project
> Anomaly detection on time series dataset using ML algorithms

An application capable of identifying anomalies out of a given dataset using Unsupervised Machine Learning Algorithms. As the nature of anomaly varies over different cases a model may not work universally for all anomaly detection problems. To increase the confidence of results, three different models LSTM-Autoencoder, Autoencoder and SUOD are used for training. Hence it is generic enough to work with variety of datasets which meet input specification. 

   
## How does it work?

This project consist of following three main components: 

#### 1. Preprocess

Path to Input data folder consisting of CSV files in S3 is provided and the input should be a time series dataset i.e. should a compulsory field for timestamp and audit_id. Dataset used is multi-dimensional categorical dataset. It starts with data-cleaning, removes the irrelevant data from dataframe, set defualt inplace of missing values and uploads the cleaned data to S3. 

Next is encoding of the dataset. Some of the categorical dataset are encoded using using binary_encoders in category-encoders library. The encoders trained using the training dataset are saved first locally and then uploaded to S3. Preprocess program accepts a flag to know if the provided dataset for preprocessing is train or test dataset. In case of test data, user should provide the location for encoders to use for encoding the dataset.

Since the dataset can be really huge. The input data is divided on the basis of similarity. To do so, K-means algorithm in FAISS — Facebook’s library for similarity search for very large datasets, is used. User can provide the number of clusters to form. Finally, the processed dataset will be divided in specified number of clusters and the clustered data is uploaded to S3.

#### 2. Train

Amazon Sagemaker is used for training all the algorithms. Implemented models are LSTM-Autoencoder, Autoencoder and KNN, IForest, LOF from SUOD. The script train creates the training job for the three algorithms and executes them. Training of Autoencoder and LSTM-Autoencoder uses tensorflow framework with the Sagemaker python SDK. 

#### 3. Inference

Predictions are generated at each step and the trained model are used to determine the resulting reconstruction error. In case of SUOD model, if majority of the models predict a data to be anomalous it is considered as an anomaly. Similarly, if majority of the models among Autoencoder, LSTM-Autoencoder and SUOD predicts a data as an anomaly it is considered as an anomaly. The ID and score for the anomalous data point is stored. The ID's of anomolous points are used to mapped to the original dataset and the resulting CSV is saved in S3. 

Mapping back the anomalous data point to the original dataset is done using AWS Athena and AWS Glue. All the columns for the anomalous data points are stored in a CSV. 

## Getting Started

#### Clone the repo (only available from source currently):

```sh
git clone https://re_cursed@bitbucket.org/re_cursed/app.git && cd project/app/
```

#### Install dependencies using **python 3.6+**:

Installing all the required dependencies that do not ship with core Python installation needed for the application.
```sh
pip install -r requirements.txt
```

#### Begin preprocessing:

Preprocess performs data cleaning, training encoders and kmeans in case of training data, encoding of the dataset and finally clustering dataset in n clusters. Start by defining arguments like the dataset location that we are going to use for preprocessing, location for storing the cleaned, encoded, clustered data and a default Amazon S3 bucket, other variables are set to some default value which can be passed as CLI agrument. Use help  command to know more about the parameters.

```sh
# run preprocess file to do data cleaning, encoding and clustering of the dataset
python -m preprocess.preprocess

# use --help or -h to display message about supported parameters
python -m preprocess.preprocess -h
```

#### Create and Execute the training jobs in Sagemaker

To run docker for creating container for SUOD model in ECR.  
```sh
# ACCOUNT_ID: account_id of user creating the ECR container needed for login
# REGION: Region of user creating the ECR container needed for login
# REPO_NAME: Repository name for ECR image 
! sh train/suod_model/build_and_push.sh <ACCOUNT_ID> <REGION> <REPO_NAME>
```

Define ECR container image uri that we are going to use for training SUOD models, default Amazon S3 bucket to be used by Amazon SageMaker etc.  Use help  command to know more about the parameters and their default values.
```sh
# run train file create and execute training jobs for all n clusters
python -m train.train

# use --help or -h to display  message about supported parameters
python -m train.train -h
```

#### Inference 

The trained models are to be downloaded in the folder data/trained_models. 
Start by defining arguments like Amazon S3 bucket, path of clustered data in S3 etc. 
```sh
# run inference file to predict anomalies in dataset for given dataset in S3
python -m inference.inference

# use --help or -h to display message about supported parameters
python -m inference.inference -h
```

#### Mapping

For mapping the anomalous data point ID back to the orginal data, AWS glue and Athena is used
```sh
# run map file for mapping
python -m inference.map

# use --help or -h to display message about supported parameters
python -m inference.map -h
```

## Algorithms Used

* LSTM Autoencoder
* Autoencoder Model
* SUOD Model
  * KNN(k nearest neighbours)
  * LOF
  * IForest

**Note:** SUOD is build at the top of pyod models and is fully compatible with the models in PyOD. However the SUOD model in this application only consists of **KNN, IForest and LOF**. It does not support all the avialable models in SUOD. Hence the user can not select any other model. References for both SUOD and pyod is attached below.

#### Folder Structure of Train 
   
    |-autoencoder_model
        |--autoencoder.py
        |--source_autoencoder.py
    |--lstm_autoencoder_model
        |--lstm_autoencoder.py
        |--source_lstm_autoencoder.py
    |--suod_model
       |-- build_and_push.sh
       |-- docker
           |-- Dockerfile
           |-- code
               |-- main.py
               |-- utils.py
               
- autoencoder.py creates the training job using tensorflow on Sagemaker for autoencoder and source_autoencoder.py is the entrypoint for the estimator.
- lstm_autoencoder.py works in the same way as autoencoder.
- Dockerfile describes how to build your Docker container image for using in SUOD training in Sagemaker.
- build_and_push.sh is a script that uses the Dockerfile to build your container images and then pushes it to ECR. We can invoke the commands directly for later use. 
- code is the directory which contains the files that are installed in the container.
- File names for rest of the folders are self explanatory and contains documentation for more details.

## References

[[1]](https://doi.org/10.1109/ATIT49449.2019.9030505)  O. I. Provotar, Y. M. Linder and M. M. Veres, "Unsupervised Anomaly Detection in Time Series Using LSTM-Based Autoencoders," 2019 IEEE International Conference on Advanced Trends in Information Theory (ATIT), Kyiv, Ukraine, 2019, pp. 513-517.

[[2]](https://doi.org/10.1145/3097983.3098052) Chong Zhou and Randy C. Paffenroth. 2017. Anomaly Detection with Robust Deep Autoencoders. In Proceedings of the 23rd ACM SIGKDD International Conference on Knowledge Discovery and Data Mining (KDD ’17). Association for Computing Machinery, New York, NY, USA, 665–674.

[[3]](https://arxiv.org/pdf/2003.05731.pdf)  Zhao, Yue, Xiyang Hu, Cheng Cheng, Cong Wang, Cao Xiao, Yunlong Wang, Jimeng Sun and Leman Akoglu. “SUOD: A Scalable Unsupervised Outlier Detection Framework.”

[[4]](https://pyod.readthedocs.io/en/latest/) Zhao, Y., Nasrullah, Z. and Li, Z., 2019. PyOD: A Python Toolbox for Scalable Outlier Detection. Journal of Machine Learning Research, 20, pp.1-7.

[[5]](https://github.com/facebookresearch/faiss/wiki?fbclid=IwAR30AzrNFJ7azoH_XnSiEuVp7SdH3EXBeQ6zDvteLTFgMTL8e7yqyGIKjOk) Faiss is a library for efficient similarity search and clustering of dense vectors. Some more [examples](https://github.com/facebookresearch/faiss/wiki/Faiss-building-blocks:-clustering,-PCA,-quantization) for better understanding of faiss.

